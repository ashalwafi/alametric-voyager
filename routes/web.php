<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@index')->name('home');
Route::get('/harga', 'PagesController@pricing')->name('pricing');
Route::get('/portfolio', 'PagesController@portfolio')->name('portfolio');
Route::get('/blog', 'BlogsController@index')->name('blog');
Route::get('/blog/{slug}', 'BlogsController@show')->name('blog.detail');
Route::get('/tentang-kami', 'PagesController@about')->name('about');
Route::get('/kebijakan-privasi', 'PagesController@policy')->name('privacy.policy');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
