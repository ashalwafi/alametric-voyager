<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $table=  'posts';
    protected $fillable = [
        'author_id',
        'category_id',
        'title',
        'seo_title',
        'excerpt'
    ];
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
}
