<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;

class BlogsController extends Controller
{
    public function index()
    {
        $posts = Post::where(['status' => 'published'])->orderBy('created_at','desc')->paginate(6);
		$popular = Post::where('status', '=', 'published')
					   ->where('created_at', '>=', 'DATE_SUB(NOW(), INTERVAL 2 MONTH)')
					   ->orderBy('view_counter','desc')->limit(7)->get();
		$random = Post::where(['status' => 'published'])->inRandomOrder()->limit(3)->get();
        return view('client.pages.blogs.index', ['posts'=>$posts, 'popular'=>$popular, 'random'=>$random]);
    }

    public function show($slug)
    {
		Post::where(['slug' => $slug ])->increment('view_counter');
        //$post = Post::where(['slug' => $slug ])->first();
		$post = DB::table('posts')
					->join('users', 'posts.author_id', '=', 'users.id')
					->select('posts.*', 'users.name')
					->where(['slug' => $slug, 'status' => 'PUBLISHED'])
					->first();		
        return view('client.pages.blogs.show', compact('post'));
    }

}
