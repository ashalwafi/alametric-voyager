@extends('client.layout.app')

@section('title',  'Platform Sosial Media Terintegrasi')

@section('content')

		<!-- Start Breadcrump Area  -->
        <div class="breadcrumb-area rn-bg-color ptb--120 bg_image bg_image--1" data-black-overlay="6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-inner pt--100 pt_sm--40 pt_md--50">
                            <h2 class="title">Alametric News</h2>
                            <ul class="page-list">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li class="current-page">News</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrump Area  -->
		<main class="page-wrapper">
            <!-- Start blog Area  -->
            <div class="rn-blog-area rn-section-gap bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title section-title--2 text-center">
                                <h2 class="title">Berita</h2>
                                <p>Kumpulan berita-berita terupdate dan terkini seputar teknologi dan bisnis.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row mt--20">
                        @foreach($posts as $post)
                        <!-- Start Blog Area  -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog blog-style--1">
                                <div class="thumbnail">
                                    <a href="{{ route('blog.detail', $post->slug ) }}">
                                        <img class="w-100" src="{{ Voyager::image( voyager_image($post->image, 'medium')  ) }}" alt="Blog Images" />
                                    </a>
                                </div>
                                <div class="content">
                                    <p class="blogtype">{{ $post->category['name'] }}</p>
                                    <h4 class="title"><a href="{{ route('blog.detail', $post->slug ) }}">{{ $post->title }}</a>
                                    </h4>
                                    <div class="blog-btn">
                                        <a class="rn-btn text-white" href="{{ route('blog.detail', $post->slug ) }}">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Blog Area  -->
                        @endforeach

                       
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            {{ $posts->links('client.components.blog-pagination', ['posts' => $posts]) }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start blog Area  -->

            <!-- Start Blog Area  -->
            <div class="rn-blog-area rn-section-gap bg_color--5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="section-title section-title--2  text-left mb--20">
                                <h2 class="title">Berita Terpopuler</h2>
                                <p>Selalu perbarui wawasan Anda dengan membaca berita-berita terpopuler dari yang kami sajikan khusus untuk anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row rn-slick-activation rn-slick-dot pb--25" data-slick-options='{
                                    "spaceBetween": 15, 
                                    "slidesToShow": 3, 
                                    "slidesToScroll": 1, 
                                    "arrows": true, 
                                    "infinite": true,
                                    "dots": true,
                                    "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "ion ion-ios-arrow-back" },
                                    "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "ion ion-ios-arrow-forward" }
                                }' data-slick-responsive='[
                                {"breakpoint":890, "settings": {"slidesToShow": 3}},
                                {"breakpoint":770, "settings": {"slidesToShow": 2}},
                                {"breakpoint":490, "settings": {"slidesToShow": 1}}
                                ]'>
						@foreach($popular as $p)
                        <!-- Start Blog Area  -->
                        <div class="blog blog-style--1">
                            <div class="thumbnail">
                                <a href="{{ route('blog.detail', $p->slug ) }}">
                                    <img class="w-100" src="{{ Voyager::image( voyager_image($p->image, 'medium')  ) }}" alt="Blog Images" />
                                </a>
                            </div>
                            <div class="content">
                                <p class="blogtype">Development</p>
                                <h4 class="title"><a href="{{ route('blog.detail', $p->slug ) }}">{{ $p->title }}</a>
                                </h4>
                                <div class="blog-btn">
                                    <a class="rn-btn text-white" href="{{ route('blog.detail', $p->slug ) }}">Read More</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Blog Area  -->
						@endforeach
                        
                    </div>
                </div>
            </div>
            <!-- End Blog Area  -->

            <!-- Start Blog Area  -->
            <div class="rn-blog-area rn-section-gap bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title section-title--2  text-center mb--20 mb_sm--0 md_sm--0">
                                <h2 class="title">Rekomendasi</h2>
                                <p>Kami sajikan hanya untuk anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
						@foreach($random as $r)
                        <!-- Start Blog Area  -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog blog-style--1">
                                <div class="thumbnail">
                                    <a href="{{ route('blog.detail', $r->slug ) }}">
                                        <img class="w-100" src="{{ Voyager::image( voyager_image($r->image, 'medium')  ) }}" alt="Blog Images" />
                                    </a>
                                </div>
                                <div class="content">
                                    <p class="blogtype">{{ $r->category['name'] }}</p>
                                    <h4 class="title"><a href="{{ route('blog.detail', $r->slug ) }}">{{ $r->title }}</a>
                                    </h4>
                                    <div class="blog-btn">
                                        <a class="rn-btn text-white" href="{{ route('blog.detail', $r->slug ) }}">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Blog Area  -->
						@endforeach
						
                    </div>
                </div>
            </div>
            <!-- End Blog Area  -->
        </main>

@endsection