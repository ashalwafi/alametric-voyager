@extends('client.layout.app')

@section('title',  $post->title)

@push('meta')

<meta property="og:title" content="{{ $post->title }}">
<meta property="og:description" content="{{ $post->excerpt }}">
<meta property="og:image" content="{{ Voyager::image( $post->image ) }}">
<meta property="og:url" content="{{ url()->current() }}">

<meta name="twitter:title" content="{{ $post->title }}">
<meta name="twitter:description" content="{{ $post->excerpt }}">
<meta name="twitter:image" content="{{ Voyager::image( $post->image ) }}">
<meta name="twitter:card" content="summary">
@endpush


@section('content')


<style>
.bg_image--1{
	background-image: url('<?= (isset($post->image)) ? Voyager::image( voyager_image( $post->image, 'default')  ) : url('../images/bg/bg-image-1.jpg') ?>');
}
</style>
	
		<!-- Start Breadcrump Area  -->
        <div class="rn-page-title-area pt--120 pb--190 bg_image bg_image--1" data-black-overlay="8">
			<!-- <img src="{{ Voyager::image( voyager_image( $post->image, 'default')  ) }}" width="1920px" height="599px" data-black-overlay="5"> -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog-single-page-title text-center pt--100">
                            <h2 class="title theme-gradient">{{ $post->title }}</h2>
                                <ul class="blog-meta d-flex justify-content-center align-items-center">
                                    <li><i data-feather="clock"></i>{{ date('d M Y', strtotime($post->created_at)) }}</li>
                                    <li><i data-feather="user"></i>{{ $post->name }}</li>
									<li><i data-feather="eye"></i>{{ $post->view_counter }}</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrump Area  -->
        <!-- Start Page Wrapper  -->
		<main class="page-wrapper">

            <!-- Start Blog Details Area  -->
            <div class="rn-blog-details pt--110 pb--70 bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-wrapper">
                                <div class="inner">
                                    <p>{{ $post->excerpt }}</p>
                                    <!-- <div class="thumbnail">
                                        <img src="{{ Voyager::image( voyager_image( $post->image, 'default')  ) }}" alt="{{ $post->title }}" >
                                    </div> -->
                                    {!! $post->body !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Blog Details Area  -->

            <!-- Start Comment Form  -->
            <div class="blog-comment-form pb--120 bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner">
                                <h3 class="title mb--40 fontWeight500">Leave a Reply</h3>
								<div class="fb-comments" data-href="{{ Request::url() }}" data-numposts="10" data-width="100%"></div> 
								<!-- <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="10" data-width="100%"></div> -->
                                <!-- <form action="#">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-12">
                                            <div class="rnform-group"><input type="text" placeholder="Name"></div>
                                            <div class="rnform-group"><input type="email" placeholder="Email"></div>
                                            <div class="rnform-group"><input type="text" placeholder="Website"></div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-12">
                                            <div class="rnform-group">
                                                <textarea placeholder="Comment"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="blog-submit-btn">
                                                <a class="rn-button-style--2 btn_solid" href="#"><span>SEND MESSAGE</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Comment Form  -->

        </main>

@endsection