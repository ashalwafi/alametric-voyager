
@extends('client.layout.app')

@section('title',  'Platform Sosial Media Terintegrasi')

@section('content')
		
		<!-- Start Breadcrump Area  -->
        <div class="breadcrumb-area rn-bg-color ptb--120 bg_image bg_image--1" data-black-overlay="6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-inner pt--100 pt_sm--40 pt_md--50">
                            <h2 class="title">Harga</h2>
                            <ul class="page-list">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li class="current-page">Harga</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrump Area  -->
		<!-- Start Page Wrapper  -->
		<main class="page-wrapper">
            <!-- Start Pricing Plan Area  -->
            <div class="rn-pricing-plan-area rn-section-gap bg_color--1">
                <div class="container">
                    <div class="row mt_dec--30">
                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h4 class="title">Professional</h4>
                                        <div class="pricing">
											<span><p style="font-size: 28px; color: #adadad; font-weight: bold; text-decoration: line-through;">Rp 4.789.900</p></span>
                                            <br/>
                                            <span class="price" style="font-size: 42px; font-weight: bold;">Rp 3.097.980</span>
                                            <span class="subtitle">/ Bulan</span>
                                        </div>
                                    </div>
                                    <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li><i class="fas fa-check" style="color:green;"></i>Posting Content 2x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Instagram Feed &amp; Story 2x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Facebook Feed &amp; Story 2x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Twitter Tweet 2x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>LinkedIn Tweet 2x / Hari</li>
											<li><i class="fas fa-check" style="color:green;"></i>Copywriting Caption</li>
											<li><i class="fas fa-check" style="color:green;"></i>Hashtag Generator</li>
											<li><i class="fas fa-times"></i>Video Motion/Explainer 1x / Bulan durasi 30 detik</li>
											<li><i class="fas fa-check" style="color:green;"></i>Facebook Ads dan Instagram Ads*</li>
											<li><i class="fas fa-times"></i>Google Ads</li>
											<li><i class="fas fa-times"></i>Posting Konten Podcast di Spotify 1x / Bulan atau di PODCAST ALA GW</li>
											<li><i class="fas fa-check" style="color:green;"></i>Posting artikel di Alametric News atau media lainnya 1x / Bulan</li>
											<li><i class="fas fa-check" style="color:green;"></i>Social Media Optimation</li>
											<li><i class="fas fa-check" style="color:green;"></i>Digital Marketing Optimation</li>
											<li><i class="fas fa-check" style="color:green;"></i>Email Broadcast 2x / Bulan Max. 500 Kontak</li>
											<li><i class="fas fa-check" style="color:green;"></i>Tim Support Service</li>
											<li><i class="fas fa-check" style="color:green;"></i>Report Bulanan</li>
                                        </ul>
                                    </div>
                                    <div class="pricing-footer">
                                        <a class="rn-btn" href="http://wppredirect.tk/go/?p=6282123601315&m=Hai%20Alametric,%20Saya%20tertarik%20dengan%20paket%20Professional.">Beli Sekarang</a>
                                        <br/><br/>
                                        <a href="terms-condition.html" style="font-size=8px;"><u>*syarat dan ketentuan berlaku</u></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing active">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h4 class="title">Business</h4>
                                        <div class="pricing">
											<span><p style="font-size: 28px; color: #adadad; font-weight: bold; text-decoration: line-through;">Rp 8.129.900</p></span>
                                            <br/>
                                            <span class="price" style="font-size: 42px; font-weight: bold;">Rp 5.198.900</span>
                                            <span class="subtitle">/ Bulan</span>
                                        </div>
                                    </div>
                                    <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li><i class="fas fa-check" style="color:green;"></i>Posting Content 3x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Instagram Feed &amp; Story 3x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Facebook Feed &amp; Story 3x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Twitter Tweet 3x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>LinkedIn Tweet 3x / Hari</li>
											<li><i class="fas fa-check" style="color:green;"></i>Copywriting Caption</li>
											<li><i class="fas fa-check" style="color:green;"></i>Hashtag Generator</li>
											<li><i class="fas fa-check" style="color:green;"></i>Video Motion/Explainer 1x / Bulan durasi 30 detik</li>
											<li><i class="fas fa-check" style="color:green;"></i>Facebook Ads dan Instagram Ads*</li>
											<li><i class="fas fa-check" style="color:green;"></i>Google Ads*</li>
											<li><i class="fas fa-check" style="color:green;"></i>Posting Konten Podcast di Spotify 1x / Bulan atau di PODCAST ALA GW</li>
											<li><i class="fas fa-check" style="color:green;"></i>Posting artikel di Alametric News atau media lainnya 3x / Bulan</li>
											<li><i class="fas fa-check" style="color:green;"></i>Social Media Optimation</li>
											<li><i class="fas fa-check" style="color:green;"></i>Digital Marketing Optimation</li>
											<li><i class="fas fa-check" style="color:green;"></i>Email Broadcast 3x / Bulan Max. 1000 Kontak</li>
											<li><i class="fas fa-check" style="color:green;"></i>Tim Support Service</li>
											<li><i class="fas fa-check" style="color:green;"></i>Report Bulanan</li>
                                        </ul>
                                    </div>
                                    <div class="pricing-footer">
                                        <a class="rn-btn" href="http://wppredirect.tk/go/?p=6282123601315&m=Hai%20Alametric,%20Saya%20tertarik%20dengan%20paket%20Business.">Beli Sekarang</a>
                                        <br/><br/>
                                        <a href="terms-condition.html" style="font-size=8px;"><u>*syarat dan ketentuan berlaku</u></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h4 class="title">Advanced</h4>
                                        <div class="pricing">
											<span><p style="font-size: 28px; color: #adadad; font-weight: bold; text-decoration: line-through;">Rp 10.289.900</p></span>
                                            <br/>
                                            <span class="price" style="font-size: 42px; font-weight: bold;">Rp 8.189.900</span>
                                            <span class="subtitle">/ Bulan</span>
                                        </div>
                                    </div>
                                    <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li><i class="fas fa-check" style="color:green;"></i>Posting Content 4x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Instagram Feed &amp; Story 4x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Facebook Feed &amp; Story 4x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Twitter Tweet 4x / Hari</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>LinkedIn Tweet 4x / Hari</li>
											<li><i class="fas fa-check" style="color:green;"></i>Copywriting Caption</li>
											<li><i class="fas fa-check" style="color:green;"></i>Hashtag Generator</li>
											<li><i class="fas fa-check" style="color:green;"></i>Video Motion/Explainer 1x / Bulan durasi 1 menit</li>
											<li><i class="fas fa-check" style="color:green;"></i>Facebook Ads dan Instagram Ads*</li>
											<li><i class="fas fa-check" style="color:green;"></i>Google Ads*</li>
											<li><i class="fas fa-check" style="color:green;"></i>Posting Konten Podcast di Spotify 2x / Bulan atau di PODCAST ALA GW</li>
											<li><i class="fas fa-check" style="color:green;"></i>Posting artikel di Alametric News atau media lainnya 4x / Bulan</li>
											<li><i class="fas fa-check" style="color:green;"></i>Social Media Optimation</li>
											<li><i class="fas fa-check" style="color:green;"></i>Digital Marketing Optimation</li>
											<li><i class="fas fa-check" style="color:green;"></i>Email Broadcast 4x / Bulan Unlimited Kontak</li>
											<li><i class="fas fa-check" style="color:green;"></i>Tim Support Service</li>
											<li><i class="fas fa-check" style="color:green;"></i>Report Bulanan</li>
                                        </ul>
                                    </div>
                                    <div class="pricing-footer">
                                        <a class="rn-btn" href="http://wppredirect.tk/go/?p=6282123601315&m=Hai%20Alametric,%20Saya%20tertarik%20dengan%20paket%20Advanced.">Beli Sekarang</a>
                                        <br/><br/>
                                        <a href="terms-condition.html" style="font-size=8px;"><u>*syarat dan ketentuan berlaku</u></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Pricing Plan Area  -->
			
			<!-- Start PROGRAM KHUSUS Area -->
            <div class="rn-pricing-plan-area rn-section-gap bg_color--1">
                <div class="container">
					<div class="row">
                        <div class="col-lg-12">
                            <div class="section-title section-title--3 text-center mb--30">
                                <h2 class="title">Program Bakti Negeri </h2>
								<p>Dikarenakan adanya dampak COVID-19 di Indonesia. Kami menyediakan paket subsidi khusus untuk UMKM &amp; Online Shop kategori menengah ke bawah selama masa pandemi.<br/>#BantuAlaGw</p>
                            </div>
                        </div>
                    </div>
                    <div class="row mt_dec--30">
						<div class="col-lg-2 col-md-6 col-12 mt--30">
						</div>	
						
                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h4 class="title">Gratis</h4>
                                        <div class="pricing">
                                            <span class="price" style="font-size: 42px; font-weight: bold;">Rp 0</span>
                                            <span class="subtitle">Max. 1 Bulan / Brand</span>
                                        </div>
                                    </div>
                                    <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li><i class="fas fa-check" style="color:green;"></i>Posting konten 8x / Bulan</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Instagram Feed &amp; Story </li>
                                            <li><i class="fas fa-times" ></i>Facebook Feed &amp; Story </li>
											<li><i class="fas fa-check" style="color:green;"></i>Team Support Service</li>
											<li><i class="fas fa-check" style="color:green;"></i>Copywriting Caption</li>
											<li><i class="fas fa-check" style="color:green;"></i>Hashtag Generator</li>
											<li><i class="fas fa-check" style="color:green;"></i>Gratis Konsul Bisnis 20 Menit</li>
											<li><i class="fas fa-times"></i>Optimasi Follower &plusmn; 1000</li>
											<li><i class="fas fa-times"></i>Report Bulanan</li>
                                        </ul>
										<br/>
                                    </div>
                                    <div class="pricing-footer">
                                        <a class="rn-btn isDisabled" style="background-color: #767676; color: #ffffff;">Kuota Penuh</a>
                                        <br/><br/>
                                        <p>KUOTA TERBATAS</p><p style="font-size:15px;">Segera Daftarkan produk/brand Anda</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h4 class="title">Survival</h4>
                                        <div class="pricing">
                                            <span class="price" style="font-size: 42px; font-weight: bold;">Rp 689.900</span>
                                            <span class="subtitle">/ Bulan</span>
                                        </div>
                                    </div>
                                    <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li><i class="fas fa-check" style="color:green;"></i>Posting konten 1x / Hari selama 26 hari aktif kerja</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Instagram Feed &amp; Story </li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Facebook Feed &amp; Story </li>
											<li><i class="fas fa-check" style="color:green;"></i>Team Support Service</li>
											<li><i class="fas fa-check" style="color:green;"></i>Copywriting Caption</li>
											<li><i class="fas fa-check" style="color:green;"></i>Hashtag Generator</li>
											<li><i class="fas fa-check" style="color:green;"></i>Gratis Konsul Bisnis 30 Menit</li>
											<li><i class="fas fa-check" style="color:green;"></i>Bonus Follower &plusmn; 1000</li>
											<li><i class="fas fa-times"></i>Report Bulanan</li>
                                        </ul>
                                    </div>
                                    <div class="pricing-footer">
                                        <a class="rn-btn" href="http://wppredirect.tk/go/?p=6282123601315&m=Hai%20Alametric,%20Saya%20tertarik%20dengan%20paket%20Survival." >Beli Sekarang</a>
									   <br/><br/><p>KUOTA TERBATAS</p><p style="font-size:15px;">Beli Sekarang Juga!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						
						<div class="col-lg-2 col-md-6 col-12 mt--30">
						</div>
							
                    </div>
                    </div>
                </div>
            </div>
            <!-- End Brand Area -->
        </main>

@endsection