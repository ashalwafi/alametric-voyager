<!-- Start Header -->
<header class="header-area formobile-menu header--transparent black-logo-version header--sticky">
            <div class="header-wrapper" id="header-wrapper">
                <div class="header-left">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('assets/images/logo/logo-blue.png') }}" alt="Alametric Logo"> <!-- Logo Perlu diganti -->
                        </a>
                    </div>
                </div>
                <div class="header-right">
                    <div class="mainmenunav d-lg-block">
                        <!-- Start Mainmanu Nav -->
                        <nav class="main-menu-navbar">
                            <ul class="mainmenu">
                                <li class="">
                                    <a href="{{ url('/#beranda') }}">Beranda</a>
                                </li>
                                <li class="">
                                    <a href="{{ route('pricing') }}">Harga</a>
                                </li>
                                <li>
                                    <a href="{{ url('/#layanan-kami') }}">Layanan Kami</a>
                                </li>
                                <li class="">
                                    <a href="{{ route('portfolio') }}">Portofolio</a>
                                </li>
                                <li class="">
                                    <a href="{{ url('/#testi-pelanggan') }}">Testimoni</a>
                                </li>
                            </ul>
                        </nav>
                        <!-- End Mainmanu Nav -->

                    </div>
                    <div class="header-btn">
                        <a class="rn-btn" href="https://forms.gle/Txn2wnin4nnBRoqF7" target="_blank">
                            <span>Coba Gratis</span>
                        </a>
                    </div>
                    <!-- Start Humberger Menu  -->
                    <div class="humberger-menu d-block d-lg-none pl--20">
                        <span class="menutrigger text-white">
                    <i data-feather="menu"></i>
                </span>
                    </div>
                    <!-- End Humberger Menu  -->
                    <!-- Start Close Menu  -->
                    <div class="close-menu d-block d-lg-none">
                        <span class="closeTrigger">
                    <i data-feather="x"></i>
                </span>
                    </div>
                    <!-- End Close Menu  -->
                </div>
            </div>
        </header>