<!-- FOOTER -->
<footer class="footer-area footer-default">
            <div class="footer-wrapper">
                <div class="row align-items-end row--0">
                    <div class="col-lg-6">
                        <div class="footer-left">
                            <div class="inner">
                                <span>SIAP UNTUK MAJU?</span>
                                <h2>Ayo Gabung <br /> Sekarang Juga!</h2>
                                    <a class="rn-button-style--2" href="{{ route('pricing') }}">
                                        <span>Pilih Layanan</span>
                                    </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="footer-right" data-black-overlay="6">
                            <div class="row">
                                <!-- Start Single Widget -->
                                <div class="col-lg-6 col-sm-6 col-12">
                                    <div class="footer-widget">
                                        <h4>Lainnya</h4>
                                        <ul class="ft-link">
                                            <li><a href="https://forms.gle/U9FQpkYuSr2qP8Vf8" target="_blank">Karir</a></li>
                                            <li><a href="{{ route('blog') }}">Berita</a></li>
											<li><a href="https://forms.gle/t2wTvFKzUPKjqhCt9" target="_blank">Kerjasama</a></li>
											<li><a href="https://forms.gle/tu4VWHhSaSu4QBQ9A" target="_blank">Acara</a></li>
											<li><a href="{{ route('about') }}">Tentang Kami</a></li>
											<li><a href="{{ route('privacy.policy') }}">Kebijakan Privasi</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Single Widget  -->
                                <!-- Start Single Widget -->
                                <div class="col-lg-6 col-sm-6 col-12 mt_mobile--30">
                                    <div class="footer-widget">
                                        <h4>Layanan Konsumen</h4>
                                        <ul class="ft-link">
                                            <li><a href="http://wppredirect.tk/go/?p=6282123601315&m=Hai%20Alametric,%20Saya%20tertarik%20dengan%20paket%20anda.">+62 821 2360 1315</a></li>
                                        </ul> 

                                        <div class="social-share-inner">
                                            <ul class="social-share social-style--2 d-flex justify-content-start liststyle mt--15">
                                                <li><a href="https://web.facebook.com/alametriccom" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="https://twitter.com/Alametriccom" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="https://www.instagram.com/alametriccom/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="https://www.linkedin.com/company/alametric" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                                <li><a href="https://www.youtube.com/channel/UC2vkwPv89237yHGV9022PBA" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Widget  -->

                                <div class="col-lg-12">
                                    <div class="copyright-text">
                                        <p>Copyright © 2020 PT Alametric Teknologi Asia. All Rights Reserved.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>